// 用来存储被注册的副作用函数的全局变量
let activeEffect;
// 存储副作用函数的数组
const bucket = new Set();
// effect函数用于注册副作用函数
function effect(fn) {
	// 将副作用函数fn赋值给activeEffect全局变量
	activeEffect = fn;
	fn();
}

const data = {
	text: "hello world",
};
// 拦截data的get/set操作
const obj = new Proxy(data, {
	get(target, key) {
		if (activeEffect) {
			bucket.add(activeEffect);
		}
		return target[key];
	},
	set(target, key, newValue) {
		target[key] = newValue;
		bucket.forEach((fn) => fn());
	},
});

// 注册副作用函数
effect(() => {
	console.log("run");
	document.body.innerHTML = obj.text;
});
// 当我更改副作用函数没有用到的属性时，副作用韩式还是会执行
// bug
setTimeout(() => {
	obj.emit = "hello vue3";
}, 1000);
