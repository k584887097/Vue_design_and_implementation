// 存储副作用函数的总结构
const bucket  = new WeakMap();
let activeEffect // 存储被注册的副作用函数

const data = {text:'hello world'}

function effect(fn) {
	// 将副作用函数fn赋值给activeEffect全局变量
	activeEffect = fn;
	fn();
}

// 拦截get/set操作
const obj = new Proxy(data,{
    get(target,key){
        // 没有被注册的副作用函数时直接终止执行
        if(!activeEffect) return target[key];
        // 根据target从bucket变量中取得depsMap，它是一个Map类型：key --> effects
        let depsMap = bucket.get(target)
        // 如果不存在depsMap，那么新建一个Map并与target关联
        if(!depsMap){
            bucket.set(target,(depsMap = new Map()))
        }
        
        // 再根据key从depsMap中取得deps，他是一个Set类型
        // 里面存储着所有与当前key相关联的副作用函数：effects
        let deps = depsMap.get(key)
        console.log('depsMap',depsMap)
        if(!deps){
            depsMap.set(key,(deps = new Set()))
        }

        deps.add(activeEffect)

        return target[key];
    },
    set(target,key,value){},
})

// 注册副作用函数
effect(() => {
	console.log(obj.text)
});
// 当我更改副作用函数没有用到的属性时，副作用函数还是会执行
// bug
// setTimeout(() => {
// 	obj.emit = "hello vue3";
// }, 1000);
