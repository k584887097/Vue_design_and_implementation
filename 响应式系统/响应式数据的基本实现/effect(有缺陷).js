// 如何实现响应式数据
// 经过思考我们发现
// 1. 当副作用函数effect执行时，会触发obj.text的读取操作
// 2. 当修改obj.text的值时，会触发obj.text的设置操作

// 根据上述分析，最终实现的步骤为
// 1. 设置一个用于存储副作用函数的bucket
// 2. 当进行数据的读取操作(副作用函数执行时)将副作用函数存储到变量bucket中
// 3. 当进行数据的设置操作时将副作用函数从变量bucket中取出并执行

// 定义一个副作用函数
function effect() {
	document.body.innerHTML = data.text;
}

// 存储副作用函数的变量
const bucket = new Set();
const obj = {
	text: "hello world",
};
// 创建一个代理对象，实现拦截对象的get/set操作
const data = new Proxy(obj, {
	get(target, key) {
		bucket.add(effect);
		return target[key];
	},
	set(target, key, newValue) {
		target[key] = newValue;
		bucket.forEach((fn) => fn());
		// 注意：上面两行的顺序不能变更，如果调换，那么在副作用函数执行时他的值还是原来的值，新值没有赋值成功
		return true;
	},
});

effect();

setTimeout(() => {
	data.text = "hello vue";
}, 1000);
