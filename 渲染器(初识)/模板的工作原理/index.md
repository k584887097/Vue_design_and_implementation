### 模板的工作原理

在 vue 中我们既可以使用虚拟 DOM(渲染函数)也可以使用模板的方式来声明式的描述真实  
对于虚拟 DOM 我们之前已经了解了，现在我们来看看模板的工作方式  
模板的工作方式，跟 vuejs 中的编译器是密不可分的  
编译器与渲染器一样只是一段程序，不过他们的内容是不同的  
渲染器是将虚拟 DOM 转换为真实 DOM，而编译器则是将模板编译为渲染函数(虚拟 DOM)

例如以下模板

```js
<div @click="handler">click me</div>
```

对于编译器来说，模板就是一个普通的字符串，它会分析该字符串并生成一个功能相同的渲染函数

```js
    render(){
        return h('div',{onclick:handler},'click me')
    }
```

再以我们熟悉 vue 文件为例，一个 vue 文件就是一个组件

```vue
<template>
	<div @click="handler">click me</div>
</template>

<script>
export default {
    data(){/*...*/ },
    methods:{
        handler() =>{/*...*/}
    },
}
</script>
```

对于以上的例子，编译器会将模板内容编译成渲染函数并添加到<script></script>标签块中

```javascript
export default {
    data(){/*....*/ },
    methods:{
        handler() =>{/*...*/}
    },
    render(){
        return h('div',{onclick:handler},'click me')
    }
}

```

综上所述，无论是使用模板还是直接手写渲染函数，对于一个组件来说，它要渲染的内容最终都是通过渲染函数产生的，然后渲染器在把渲染函数返回的虚拟 DOM 渲染为真实 DOM，这就是模板的工作原理，也是 vue 的渲染页面的流程

至于编译器是如何将模板编译编译为渲染函数，这是一个比较大的话题，在之后在了解(咕咕咕)
