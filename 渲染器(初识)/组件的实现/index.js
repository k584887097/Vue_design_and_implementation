import mountElment from "../普通的虚拟DOM/rende.js";

// 虚拟DOM出来描述真是DOM以外，还能够描述组件
// 组件的本质：组件就是一组虚拟DOM元素的封装

// 为了是实现组件定义DOM，我们定义一个函数用来转换为真实DOM
function render(vnode, container) {
	if (typeof vnode.target === "string") {
		// 如果vnode的标签名称是字符串类型，那么描述的是不同元素
		mountElment(vnode, container);
	}
	if (typeof vnode.target === "object") {
		// 如果vnode的标签名称是对象类型，那么描述的是组件

		// 因为组件出虚拟DOM还有其他的作用，例如emits、props等
		// mountElment(vnode.target, container);

		mountComponent(vnode, container);
	}
}

// 定义一个函数用来处理组件
function mountComponent(vnode, container) {
	const subtree = vnode.target.render();
	mountElment(subtree, container);
}
export default render;
