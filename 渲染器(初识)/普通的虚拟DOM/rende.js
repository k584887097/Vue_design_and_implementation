// 渲染器就是能将虚拟DOM渲染成真是DOM的代码

// 定义一个函数，将传进来的虚拟DOM对象转换为真实DOM
// vnode 传输的虚拟DOM对象
// container 真是DOM对象当作挂载点
function render(vnode, container) {
	// 创建一个用vnode.target为标签名称的标签
	const el = document.createElement(vnode.target);
	// 便利props将属性、时间添加到DOM元素上
	for (const key in vnode.props) {
		if (/^on/.test(key)) {
			// 如果key以on开头，说明它是事件
			el.addEventListener(key.substring(2).toLowerCase(), vnode.props[key]);
		} else if (el.style.hasOwnProperty(key)) {
			// 判断key是否是为css样式，如果是则设置样式
			el.style[key] = vnode.props[key];
		} else {
			// 以上都不是的情况
			el.setAttribute(key, vnode.props[key]);
		}
	}
	// 处理children
	if (typeof vnode.children === "string") {
		el.appendChild(document.createTextNode(vnode.children));
	} else if (Array.isArray(vnode.children)) {
		for (const child of vnode.children) {
			render(child, el);
		}
	}
	container.appendChild(el);
}

export default render;
